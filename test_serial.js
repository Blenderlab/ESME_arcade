var SerialPort = require('serialport');
var port = new SerialPort('/dev/ttyAMA0', { autoOpen: false , baudRate:9600, dataBits: 8,
  parity: 'none',
  stopBits: 1,flowControl:true});

port.open(function (err) {
  if (err) {
    return console.log('Error opening port: ', err.message);
  };
  console.log('ok');  // write errors will be emitted on the port since there is no callback to write
  port.write('main screen turn on');
});

// the open event will always be emitted
port.on('open', function() {
  console.log('port ouvert');
});

port.on('data', function(data) {
   console.log('data=',data);
});

port.on('readable', function() {
   console.log('read=',port.read());
});
