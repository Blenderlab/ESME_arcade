var express = require("express");
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var SerialPort = require('serialport');
var lastkey="";
var message="";
var compt=0;
const util = require('util')

var port = new SerialPort('/dev/ttyACM1', { autoOpen: false , baudRate:9600});

var db = require("./modules/db.js");

const parsers = SerialPort.parsers; 
const parser = new parsers.Readline({
        delimiter : '\r\n'
});

port.pipe(parser);


var states = {
    registration: false
};

app.use("/", express.static('public'));

var clients = io.on('connection', function(socket){
    console.log('USER CONNECTED');

    socket.on('disconnect', function(){
        console.log('USER DISCONNECTED');
    });

    socket.on('user:add', function(data){
        db.addUser(data,function(){
            socket.emit("user:add:ok");
        })
    });

    socket.on('user:registration', function(){
        states.registration = true;
        console.log('USER registration');
    });

    socket.on('user:delete', function(rowid){
        db.deleteUser(rowid,function(){
            socket.emit("user:delete:ok");
        })
    });

    socket.on('user:fetch-list', function(){
        db.fetchUsers(function(err,rows){
            if(err === null){
                socket.emit("user:list",rows);
            }
        });
    });

    socket.on('events:fetch-list', function(){
        db.fetchEvents(function(err,rows){
            if(err === null){
                socket.emit("events:list",rows);
            }
        });
    });
});

port.open(function (err) {
  if (err) {
    return console.log('Error opening port: ', err.message);
  };
  console.log('Port Opened !');  // write errors will be emitted on the port since there is no callback to write
});

// the open event will always be emitted
port.on('open', function() {
  console.log('Opening Port...');
});

parser.on('data', function(data) {		
	if(states.registration === true){
            clients.emit('reader:get', {userId:data});
            states.registration = false;
        }else{
            if (lastkey===data){
		return;    
	    } else {
		lastkey=data;
	        db.getUser(data, function(error, result){
                if(result.permission === "denied"){
			console.log(data, ' DENIED');
                }else{
			console.log(data,' GRANTED');
                }


                db.writeEvent(result, function(err, result){
                    if(err === null){
                        clients.emit('events:add', result);
                    }
                });

            });
}
        }

});

http.listen(3001,function(){
    console.log("LISTENING");
});
